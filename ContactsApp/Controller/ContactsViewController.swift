//
//  ContactsViewController.swift
//  ContactsApp
//
//  Created by Adrian Dróżdż on 24/05/2019.
//  Copyright © 2019 Adrian Dróżdż. All rights reserved.
//

import UIKit
import CoreData

class ContactsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var fetchedResultsController: NSFetchedResultsController<NSFetchRequestResult>!
    
    var firstNameTextField: UITextField!
    var lastNameTextField: UITextField!
    var phoneNumberTextField: UITextField!
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Contact")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "lastName", ascending: true)]
        
        fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: AppDelegate.context, sectionNameKeyPath: nil, cacheName: nil)
        fetchedResultsController.delegate = self
        
        do {
            try fetchedResultsController.performFetch()
        } catch {
            print("Could not fetch contacts, \(error.localizedDescription)")
        }
        
        tableView.tableFooterView = UIView()
    }
    
    //MARK: - TableView DataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchedResultsController.sections?[0].numberOfObjects ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactCell", for: indexPath) as! ContactTableViewCell
        
        let contact = fetchedResultsController.object(at: indexPath)
        cell.setupCell(contact: contact as! Contact)
        
        return cell
    }
    
    //MARK: - TableView Delegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        performSegue(withIdentifier: "contactsToProfileSeg", sender: indexPath)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        let contactToDelete = fetchedResultsController.object(at: indexPath) as! Contact
        AppDelegate.context.delete(contactToDelete);
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
    }
    
    //MARK: - Segue Methods
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        switch segue.identifier {
        case "contactsToProfileSeg":
            let destinationVC = segue.destination as! ProfileViewController
            let indexPath = sender as! IndexPath
            let tempContact = fetchedResultsController.object(at: indexPath)
            
            destinationVC.localContact = tempContact as! Contact
            break
        default:
            return
        }
    }
    
}

extension ContactsViewController: NSFetchedResultsControllerDelegate {
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        if type == .insert {
            
        }
        
        switch type {
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .automatic)
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .automatic)
        default:
            print("Unkown type")
        }
    }
}
