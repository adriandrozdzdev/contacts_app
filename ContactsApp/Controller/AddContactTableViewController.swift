//
//  AddContactTableViewController.swift
//  ContactsApp
//
//  Created by Adrian Dróżdż on 30/05/2019.
//  Copyright © 2019 Adrian Dróżdż. All rights reserved.
//

import UIKit
import CoreData

class AddContactTableViewController: UITableViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var dateOfBirthTextField: UITextField!
    @IBOutlet weak var addressTextView: UITextView!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    
    var imagePicker = UIImagePickerController()
    var avatarImage: UIImage?
    var datePicker = UIDatePicker()
    var dateOfBirth: Date?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //remove empty cells
        tableView.tableFooterView = UIView()
        
        createDatePicker()
        imagePicker.delegate = self
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {

        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return 3
    }
    
    //MARK: - TableView Delegates
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return ""
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if (section == 1) {
            return 20
        }
        
        return 0
    }
    
    //MARK: - IBActions
    
    @IBAction func cancelButtonPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveButtonPressed(_ sender: UIButton) {
        
        if firstNameTextField.text != "" && lastNameTextField.text != "" && phoneNumberTextField.text != "" {
            createNewContact()
        } else {
            showWarning(name: "Validation failed", text: "Please insert name, surname and a phone numer!")
        }
        
    }
    
    @IBAction func cameraTapped(_ sender: UITapGestureRecognizer) {
        
        showAlert()
    }
    
    @objc func datePickerChangedValue() {
        dateOfBirthTextField.text = dateFormatter().string(from: datePicker.date)
        dateOfBirth = datePicker.date
    }
    
    //MARK: - Saving Contacts
    
    func createNewContact() {
        
        let context = AppDelegate.context
        let newContact = Contact(context: context)
        
        newContact.firstName = firstNameTextField.text
        newContact.lastName = lastNameTextField.text
        newContact.fullName = firstNameTextField.text! + " " + lastNameTextField.text!
        newContact.phoneNumber = phoneNumberTextField.text
        
        if avatarImage != nil {
            newContact.avatar = avatarImage!.jpegData(compressionQuality: 0.7 )
        }

        if addressTextView.text != "" {
            newContact.address = addressTextView .text
        }

        if dateOfBirth != nil {
            newContact.dateOfBirth = dateOfBirth!
        }
        
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - AlertController
    
    func showAlert() {
        
        let alert = UIAlertController(title: "Choose avatar", message: nil, preferredStyle: .actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: .default) { (alert) in
            
            self.showCamera()
        }
        let galleryAction = UIAlertAction(title: "Gallery", style: .default) { (alert) in
            
            self.showGallery()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alert.addAction(cameraAction)
        alert.addAction(galleryAction)
        alert.addAction(cancelAction)
        
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = avatarImageView
            alert.popoverPresentationController?.sourceRect = avatarImageView.bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
            break
        default:
            break
        }
        
        present(alert, animated: true)
    }
    
    //MARK: - Helpers
    
    func showCamera() {
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = false
            present(imagePicker, animated: true, completion: nil)
        } else {
            showWarning(name: "Warning", text: "No camera found!")
        }
    }
    
    func showGallery() {
        
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = false
        present(imagePicker, animated: false, completion: nil)
    }
    
    func showWarning(name: String, text: String?)
    {
        let alert = UIAlertController(title: name, message: text, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
    func  createDatePicker() {
        
        datePicker.datePickerMode = .date
        datePicker.addTarget(self, action: #selector(self.datePickerChangedValue), for: .valueChanged)
        dateOfBirthTextField.inputView = datePicker
        
    }
    
    //MARK: - UIImagePickerController Delegate Methods
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let tempImage = info[.originalImage] {
            avatarImage = tempImage as? UIImage
            //keep aspect ratio
            avatarImageView.contentMode = .scaleAspectFit
            avatarImageView.image = avatarImage
        }
        
        imagePicker.dismiss(animated: false, completion: nil)
        
    }
    
}
