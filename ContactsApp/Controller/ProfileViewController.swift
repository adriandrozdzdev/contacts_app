//
//  ProfileViewController.swift
//  ContactsApp
//
//  Created by Adrian Dróżdż on 29/05/2019.
//  Copyright © 2019 Adrian Dróżdż. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var dateOfBirthLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    var localContact: Contact!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateUI()
    }
    
    //MARK: - Update UI
    func updateUI() {
        if localContact != nil {
            self.title = localContact.fullName
            phoneNumberLabel.text = "Phone: " + localContact.phoneNumber!
            dateOfBirthLabel.text = "Date of birth: " + dateFormatter().string(from: localContact.dateOfBirth!)
            
            if localContact.avatar != nil {
                avatarImageView.image = UIImage(data: localContact.avatar!)
            }
            
            if localContact.address != nil {
                addressLabel.text = "Address: " + localContact.address!
            }
            
        }
        
    }


}
