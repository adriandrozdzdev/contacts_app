//
//  ContactTableViewCell.swift
//  ContactsApp
//
//  Created by Adrian Dróżdż on 31/05/2019.
//  Copyright © 2019 Adrian Dróżdż. All rights reserved.
//

import UIKit

class ContactTableViewCell: UITableViewCell {
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setupCell(contact: Contact) {
        
        fullNameLabel.text = contact.fullName
        phoneNumberLabel.text = contact.phoneNumber
        
        if contact.avatar != nil {
            avatarImageView.image = UIImage(data: contact.avatar!)
        }
    }
    
    

}
