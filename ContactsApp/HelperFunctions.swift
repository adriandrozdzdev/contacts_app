//
//  HelperFunctions.swift
//  ContactsApp
//
//  Created by Adrian Dróżdż on 02/06/2019.
//  Copyright © 2019 Adrian Dróżdż. All rights reserved.
//

import Foundation

let dateFormat = "dd/MM/yyyy"

func dateFormatter() -> DateFormatter {
    
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = dateFormat
    
    return dateFormatter
}
